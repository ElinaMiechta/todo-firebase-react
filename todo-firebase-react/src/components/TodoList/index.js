import { useState } from "react";

import "./index.css";
import ListAltIcon from "@material-ui/icons/ListAlt";
import Todo from "../Todo";
import { useTodoWithReducer } from "../../hooks/useTodoWithReducer";
import ModalAddTodo from "../ModalWindow/ModalAddTodo";
import { ACTIONS } from "../../hooks/useTodoWithReducer";

const TodoList = ({ user }) => {
  const [openEditTodo, setOpenEditTodo] = useState(false);
  const [currentId, setCurrentId] = useState(null);

  const {
    dispatch,
    text,
    addTodoText,
    edit,
    todos,
    toggleComplete,
  } = useTodoWithReducer();

  const clickDetector = (id) => {
    setCurrentId(id);
    setOpenEditTodo(true);
  };

  return (
    <div className="todosContainer">
      <ModalAddTodo
        open={openEditTodo}
        text={text}
        title="Edit Todo"
        handleClose={() => setOpenEditTodo(false)}
        handleChange={(e) => addTodoText(e)}
        handleSubmit={(e) => edit(e, currentId)}
      />
      <div className="todosContainer__heading">
        <h2>
          Your Todo List <ListAltIcon className="todosContainer__icon" />
        </h2>
      </div>
      <div className="todosContainer__content">
        <ul className="todosContainer__content--list">
          {todos &&
            todos.map(({ todo, id }) => (
              <Todo
                key={id}
                text={todo.text}
                isComplete={todo.complete}
                onChangeCx={() => toggleComplete(id)}
                handleRemove={() =>
                  dispatch({
                    type: ACTIONS.REMOVE_TODO,
                    payload: { key: id, username: user.displayName },
                  })
                }
                openModalEdit={() => setOpenEditTodo(true)}
                clickListener={() => clickDetector(id)}
              />
            ))}
        </ul>
      </div>
    </div>
  );
};

export default TodoList;
