import "./index.css";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import PersonIcon from "@material-ui/icons/Person";
import { Button } from "@material-ui/core";

const Header = ({ handleOpen, handleOpenLogin, user, logout }) => {
  return (
    <>
      <header className="header">
        <img
          src="https://images.unsplash.com/photo-1506905925346-21bda4d32df4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
          alt="mountains"
        />
        <div className="header--container">
          {user ? (
            <>
              <div className="header__title">
                <h1>Your things, {user.displayName}</h1>
              </div>
              <div className="header__btns">
                <Button className="header__btns--signup" onClick={logout}>
                  <ExitToAppIcon />
                  Log out
                </Button>
              </div>
            </>
          ) : (
            <>
              <div className="header__title">
                <h1>Manage Your time with Todo App!</h1>
              </div>
              <div className="header__btns">
                <Button className="header__btns--signup" onClick={handleOpen}>
                  <AccountCircleIcon />
                  Sign up
                </Button>
                <Button
                  className="header__btns--signin"
                  onClick={handleOpenLogin}>
                  <PersonIcon />
                  Sign in
                </Button>
              </div>
            </>
          )}
        </div>
      </header>
    </>
  );
};

export default Header;
