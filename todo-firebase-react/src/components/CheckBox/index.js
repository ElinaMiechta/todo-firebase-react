import Checkbox from "@material-ui/core/Checkbox";
import { withStyles } from "@material-ui/core/styles";
import FormControlLabel from "@material-ui/core/FormControlLabel";

function CheckBox({ isChecked, handleChange }) {
  return (
    <FormControlLabel
      control={
        <Checkbox
          checked={isChecked}
          onChange={handleChange}
          name="checkedB"
          color="primary"
        />
      }
    />
  );
}

export default CheckBox;
