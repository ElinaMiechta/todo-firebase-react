import EditIcon from '@material-ui/icons/Edit';
import CheckBox from "../CheckBox";
import DeleteIcon from '@material-ui/icons/Delete';


const Todo = ({ text, handleRemove, isComplete, onChangeCx, clickListener }) => {

  return (
    <li className="list__item">
      <CheckBox handleChange={onChangeCx}/>
      <p className={isComplete ? 'list__item--text checked' : 'list__item--text'}>{text}</p>
      <EditIcon  className="list__item--icon__edit" onClick={clickListener}/>
      <DeleteIcon  className="list__item--icon" onClick={handleRemove}/>
    </li>
  );
};

export default Todo;
