import {useState} from 'react';

import './index.css'
import AddCircleIcon from '@material-ui/icons/AddCircle';

const AddTodo = ({openModal}) => {
      return (
            <div className="todoAdd">
                  <div className="todoAdd__icon--wrapper">
                        <AddCircleIcon 
                        className="todoAdd__icon"
                        onClick={openModal}
                        />
                  </div>  
            </div>
      )
}

export default AddTodo
