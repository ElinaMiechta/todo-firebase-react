import "./index.css";
import AvTimerIcon from "@material-ui/icons/AvTimer";
import AccessibilityNewIcon from "@material-ui/icons/AccessibilityNew";
import EditAttributesIcon from "@material-ui/icons/EditAttributes";

const Welcome = () => {
  return (
    <section className="welcome">
                      <h1>Login to start!...</h1>
      <div className="welcome--container">
        <div>
          <AvTimerIcon className="welcome__icon"/>
          <h2>Time management</h2>
        </div>
        <div>
          <AccessibilityNewIcon className="welcome__icon"/>
          <h2>Fun</h2>
        </div>
        <div>
          <EditAttributesIcon className="welcome__icon"/>
          <h2>Easy</h2>
        </div>
      </div>
    </section>
  );
};

export default Welcome;
