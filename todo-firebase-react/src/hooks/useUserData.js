import { useState, useEffect } from "react";

import { auth, db } from "../firebaseConfiq";

export const useUserData = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [user, setUser] = useState(null);
  const [openSignIn, setOpenSignIn] = useState(false);
  const [dbUser, setDbUser] = useState(null);

  const closeModal = () => setOpenSignIn(false);
  const openSignInModal = () => setOpenSignIn(true);

  const handleChange = (e) => {
    const { name, value } = e.target;
    switch (name) {
      case "username":
        setUsername(value);
        break;

      case "password":
        setPassword(value);
        break;

      case "email":
        setEmail(value);
    }
  };

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        setUser(authUser);
        setDbUser(authUser.displayName);
      } else {
        setUser(null);
      }
    });

    return () => {
      unsubscribe();
    };
  }, [user, username, dbUser]);

  const signup = (e) => {
    e.preventDefault();
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((authUser) => {
        authUser.user.updateProfile({
          displayName: username,
        });
      })
      .catch((error) => alert(error.message));

    db.collection("users").add({
      username: username,
    });
  };

  const logout = () => auth.signOut();

  const signin = (e) => {
    e.preventDefault();
    auth
      .signInWithEmailAndPassword(email, password)
      .catch((error) => alert(error.message));
    closeModal();
  };

  return {
    signin,
    signup,
    logout,
    handleChange,
    user,
    username,
    password,
    email,
    openSignIn,
    closeModal,
    openSignInModal,
    dbUser,
  };
};
