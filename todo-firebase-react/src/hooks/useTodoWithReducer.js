import { useState, useEffect, useReducer } from "react";

import { db } from "../firebaseConfiq";
import { useUserData } from "./useUserData";
import firebase from "firebase";

export const ACTIONS = {
  ADD_TODO: "add-todo",
  EDIT_TODO: "edit-todo",
  REMOVE_TODO: "remove-todo",
  TOGGLE_COMPLETE: "toggle-complete",
};

const reducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.ADD_TODO:
      db.collection("users")
        .doc(action.payload.username)
        .collection("todos")
        .add({
          text: action.payload.text,
          timestamp: firebase.firestore.FieldValue.serverTimestamp(),
          complete: action.payload.complete,
        });
      break;
    case ACTIONS.REMOVE_TODO:
      db.collection("users")
        .doc(action.payload.username)
        .collection("todos")
        .doc(action.payload.key)
        .delete()
        .then(() => {
          console.log("Document successfully deleted!");
        })
        .catch((error) => {
          console.error("Error removing document: ", error);
        });
      break;
    case ACTIONS.EDIT_TODO:
      db.collection("users")
        .doc(action.payload.username)
        .collection("todos")
        .doc(action.payload.key)
        .update({
          text: action.payload.text,
        });
      break;
    case ACTIONS.TOGGLE_COMPLETE:
      db.collection("users")
        .doc(action.payload.username)
        .collection("todos")
        .doc(action.payload.key)
        .update({
          complete: !action.payload.complete,
        });
  }
};

export const useTodoWithReducer = () => {
  const [todos, setTodos] = useState([]);
  const [text, setText] = useState("");
  const { user, dbUser } = useUserData();
  const [complete, setComplete] = useState(false);
  const [state, dispatch] = useReducer(reducer, todos);

  useEffect(() => {
    if (dbUser) {
      db.collection("users")
        .doc(dbUser)
        .collection("todos")
        .orderBy("timestamp", "desc")
        .onSnapshot((snapshot) => {
          setTodos(
            snapshot.docs.map((doc) => ({
              id: doc.id,
              todo: doc.data(),
            }))
          );
        });
    } else {
      console.log("no user");
    }
  }, [dbUser]);

  const add = (e) => {
    e.preventDefault();
    dispatch({
      type: ACTIONS.ADD_TODO,
      payload: {
        username: user.displayName,
        text: text,
        complete: complete,
      },
    });
    setText("");
  };

  const edit = (e, key) => {
    e.preventDefault();
    dispatch({
      type: ACTIONS.EDIT_TODO,
      payload: { username: user.displayName, key: key, text: text },
    });
    setText("");
  };

  const toggleComplete = (key) => {
    dispatch({
      type: ACTIONS.TOGGLE_COMPLETE,
      payload: {
        username: user.displayName,
        key: key,
        complete: complete,
      },
    });

    setComplete(!complete);
  };

  const addTodoText = (e) => {
    setText(e.target.value);
  };

  return {
    todos,
    add,
    addTodoText,
    text,
    dispatch,
    edit,
    todos,
    toggleComplete,
  };
};
