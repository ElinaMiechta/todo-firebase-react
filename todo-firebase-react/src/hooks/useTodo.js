import { useState, useEffect } from "react";

import { db } from "../firebaseConfiq";
import { useUserData } from "./useUserData";
import firebase from "firebase";

export const useTodo = () => {
  const [todos, setTodos] = useState([]);
  const [text, setText] = useState("");
  const { user, dbUser } = useUserData();
  const [complete, setComplete] = useState(false);

  useEffect(() => {
    if (dbUser) {
      db.collection("users")
        .doc(dbUser)
        .collection("todos")
        .orderBy("timestamp", "desc")
        .onSnapshot((snapshot) => {
          setTodos(
            snapshot.docs.map((doc) => ({
              id: doc.id,
              todo: doc.data(),
            }))
          );
        });
    } else {
      console.log("no user");
    }
  }, [dbUser]);

  const handleAddingText = (e) => {
    setText(e.target.value);
  };

  const addTodo = (e) => {
    e.preventDefault();

    db.collection("users").doc(user.displayName).collection("todos").add({
      text: text,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
      complete: complete,
    });
    setText("");
  };

  const removeTodo = (key) => {
    db.collection("users")
      .doc(user.displayName)
      .collection("todos")
      .doc(key)
      .delete()
      .then(() => {
        console.log("Document successfully deleted!");
      })
      .catch((error) => {
        console.error("Error removing document: ", error);
      });
  };

  const handleCheckboxChange = (key) => {
    db.collection("users")
      .doc(user.displayName)
      .collection("todos")
      .doc(key)
      .update({
        complete: !complete,
      });

    setComplete(!complete);
  };

  const editTodo = (e, key) => {
    e.preventDefault();
    db.collection("users")
      .doc(user.displayName)
      .collection("todos")
      .doc(key)
      .update({
        text: text,
      });
    setText("");
  };

  return {
    todos,
    addTodo,
    handleAddingText,
    text,
    removeTodo,
    handleCheckboxChange,
    editTodo,
  };
};
