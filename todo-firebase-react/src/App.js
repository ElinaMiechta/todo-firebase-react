import { useState } from "react";

import Header from "./components/Header";
import ModalWindow from "./components/ModalWindow";
import ModalAddTodo from "./components/ModalWindow/ModalAddTodo";
import TodoList from "./components/TodoList";
import AddTodo from "./components/AddTodo";
import { useUserData } from "./hooks/useUserData";
import Welcome from "./components/Welcome";
import { useTodoWithReducer } from "./hooks/useTodoWithReducer";

function App() {
  const [open, setOpen] = useState(false);
  const [openAddTodo, setOpenAddTodo] = useState(false);
  const {
    handleChange,
    username,
    email,
    password,
    user,
    openSignIn,
    closeModal,
    openSignInModal,
    signin,
    signup,
    logout,
  } = useUserData();
  const { add, addTodoText, text, dispatch } = useTodoWithReducer();

  return (
    <div className="app">
      <Header
        handleOpen={() => setOpen(true)}
        handleOpenLogin={openSignInModal}
        logout={logout}
        user={user}
      />
      <ModalWindow
        open={open}
        handleClose={() => setOpen(false)}
        title="Start planning!.."
        username={username}
        password={password}
        email={email}
        handleChange={(e) => handleChange(e)}
        handleSubmit={(e) => signup(e)}
      />

      <ModalWindow
        open={openSignIn}
        handleClose={closeModal}
        title="Check Your Plans!.."
        username={username}
        password={password}
        email={email}
        handleChange={(e) => handleChange(e)}
        handleSubmit={(e) => signin(e)}
      />

      <ModalAddTodo
        open={openAddTodo}
        text={text}
        title="Add Todo"
        handleClose={() => setOpenAddTodo(false)}
        handleChange={(e) => addTodoText(e)}
        handleSubmit={(e) => add(e)}
      />
      {user ? (
        <>
          <TodoList user={user} />
          <AddTodo openModal={() => setOpenAddTodo(true)} />
        </>
      ) : (
        <Welcome />
      )}
    </div>
  );
}

export default App;
